
#include <unistd.h> // getpid
#include <cstdint> // fixed uint types
#include <cstdlib> // atoi
#include <cstdio> // printf
#include <cstring> // memset
#include <signal.h> // signal
#include <errno.h> // errno global
#include <sys/ipc.h> // ftok key_t
#include <sys/msg.h> // msgget
#include <sys/mman.h> // mmap
#include <sys/wait.h> // wait
#include <fcntl.h> // shm_open flags
#include <type_traits> // underlying_type
#include <unordered_map>
#include <string>

#include <experimental/source_location>

// compile with
// g++ ping_pong_one_more_time.cpp -o ppong -lrt -Wno-narrowing -Wno-write-strings
// run with
// ./ppong <player_amount> <rounds_of_ball_feed>

// kill -l -> list all availble signals
// kill -s <signal> <pid>
// ipcs
// ipcrm -q 


void log_sys_err_and_exit(const std::experimental::source_location& location) {
    printf("%s:%d <---sysErr---> %s\n", location.file_name() , location.line(), strerror(errno)); 
    std::exit(EXIT_FAILURE);  
}

struct DataReceivedFromChild {
    std::string ftok_first;
    uint32_t ftok_second;
    pid_t pid;
};


uint32_t number_of_rounds = 0;
pid_t root_process = -1;
DataReceivedFromChild data_process_without_child{"NULL", -1, -1};
DataReceivedFromChild child_data;
std::pair<std::string, uint32_t> current_ftok{"./ppong", 21};

std::pair<std::string, uint32_t> parrent_ftok;

uint32_t ball_received = 0;
uint32_t ball_send = 0;

struct PassBallMessage {
  long type;
  char payload[100];
  int pid_of_receiver;
};


enum class PassBallMessageType : long { // have to be type of long
    forward = 4, // type of message used in message queue have to be greater than 0
    backward = 5
};

using Message = char*;
std::unordered_map<PassBallMessageType, Message> messageTypeMap {
    {PassBallMessageType::forward, "moving ball forward"}, // -Wno-write-strings
    {PassBallMessageType::backward, "moving ball backward"} // -Wno-write-strings
};


void read_statistic(pid_t pid) {
    std::string shm_name = std::string{"shared_"} + std::to_string(pid);

    int fd_shm = shm_open(shm_name.c_str(), O_CREAT | O_RDWR, 0666);
    if(fd_shm == -1) log_sys_err_and_exit(std::experimental::source_location::current());

    const int size = getpagesize();
    void* mem_ptr = mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd_shm, 0);
    if(mem_ptr == MAP_FAILED) log_sys_err_and_exit(std::experimental::source_location::current());

    auto ptr = reinterpret_cast<uint32_t*>(mem_ptr);
    printf("ball sended: %d\n", *ptr);
    ptr++;
    printf("ball received: %d\n", *ptr);

    munmap(mem_ptr, size);
}

void feed_ball() {
    key_t key{ ftok(child_data.ftok_first.c_str(), child_data.ftok_second)};
    if(key == -1) log_sys_err_and_exit(std::experimental::source_location::current());

    uint32_t queue_id{ msgget(key, 0666 | IPC_CREAT)};
    if(queue_id == -1) log_sys_err_and_exit(std::experimental::source_location::current());

    PassBallMessage msg;
    msg.type = static_cast<std::underlying_type<PassBallMessageType>::type>(PassBallMessageType::forward);
    strcpy(msg.payload, messageTypeMap.at(PassBallMessageType::forward));
    msg.pid_of_receiver = child_data.pid;

    if( msgsnd(queue_id, &msg, sizeof(msg), IPC_NOWAIT) == -1) log_sys_err_and_exit(std::experimental::source_location::current());
        printf("I'm root process %d Feed ball into game to my child %d\n", getpid(), child_data.pid);
    ball_send++;
}

#ifdef __cplusplus // since whole code is compiled by g++ not gcc, this cond is not needed
extern "C" { // signal handler should be threated by compiler as C code
#endif
void feed_ball_to_the_process_sigusr1_handler(int sig_no) {
    if(sig_no == SIGUSR1) {
        printf("SIGUSR1 handled\n");
        feed_ball();
        return;
    }
    printf("invalid signal number passed to handler\n");
}
void read_statistic_sigusr2_handler(int sig_no) {
    if(sig_no == SIGUSR2) {
        printf("SIGUSR2 handled\n");
        read_statistic(getpid());
        return;
    }
    printf("invalid signal number passed to handler\n");
}
#ifdef __cplusplus
}
#endif

void register_signal_handler_game_init() {
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = &feed_ball_to_the_process_sigusr1_handler;
    if(sigaction(SIGUSR1, &sa, NULL) == -1) log_sys_err_and_exit(std::experimental::source_location::current());
}

void register_signal_handler_get_stats() {
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = &read_statistic_sigusr2_handler;
    if(sigaction(SIGUSR2, &sa, NULL) == -1) log_sys_err_and_exit(std::experimental::source_location::current());
}

DataReceivedFromChild spawn_ping_pong_player_process(uint32_t player_amount) {
    pid_t child_pid;
    for(uint32_t player_no = 0; player_no < player_amount; player_no++)
    {
        child_pid = fork();

        uint32_t child_ftok_second = player_amount + rand()%100;
        if(child_pid == -1) log_sys_err_and_exit(std::experimental::source_location::current());
        if(child_pid != 0) 
            return DataReceivedFromChild{ "./ppong", child_ftok_second, child_pid };
        if(child_pid == 0)
        {
            printf("created new child player process %d of parrent %d\n", getpid(), getppid());
            parrent_ftok.first = current_ftok.first;
            parrent_ftok.second = current_ftok.second;


            current_ftok.first = "./ppong";
            current_ftok.second = child_ftok_second;
        }

    }
    return data_process_without_child;
}

void update_statistic(pid_t pid) {
    std::string shm_name = std::string{"shared_"} + std::to_string(pid);

    int fd_shm = shm_open(shm_name.c_str(), O_CREAT | O_RDWR, 0666);
    if(fd_shm == -1) log_sys_err_and_exit(std::experimental::source_location::current());

    ftruncate(fd_shm, 2*sizeof(uint32_t));

    const int size = getpagesize();
    auto mem_ptr = mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd_shm, 0);
    if(mem_ptr == MAP_FAILED) log_sys_err_and_exit(std::experimental::source_location::current());

    auto casted_mem_ptr = reinterpret_cast<uint32_t*>(mem_ptr);
    memcpy(casted_mem_ptr, &ball_send, sizeof(uint32_t));
    casted_mem_ptr++;
    memcpy(casted_mem_ptr, &ball_received, sizeof(uint32_t));

    munmap(mem_ptr, size);
    printf("statistic updated\n");
}

int main(int argc, char** argv)
{
    if(argc < 3) {
        printf("too few arguments\n");
        std::exit(EXIT_FAILURE);
    }
    uint32_t additional_players_amount{atoi(argv[1])-1}; // -Wno-narrownig 
    number_of_rounds = atoi(argv[2]); // -Wno-narrowing
    uint32_t current_round =0;

    register_signal_handler_game_init();
    register_signal_handler_get_stats();
    root_process = getpid();
    printf("root pid process %d, console pid %d\n", getpid(), getppid());

    child_data = spawn_ping_pong_player_process(additional_players_amount);
    update_statistic(getpid());

    key_t child_key, current_key, parrent_key;
    uint32_t child_queue_id, current_queue_id, parrent_queue_id;

    if(child_data.pid != -1)
    {  
        child_key = ftok(child_data.ftok_first.c_str(), child_data.ftok_second);
        if(child_key == -1) log_sys_err_and_exit(std::experimental::source_location::current());

        child_queue_id = msgget(child_key, 0666 | IPC_CREAT);
        if(child_queue_id == -1) log_sys_err_and_exit(std::experimental::source_location::current()); 
    }

    current_key = ftok(current_ftok.first.c_str(), current_ftok.second);
    if(current_key == -1) log_sys_err_and_exit(std::experimental::source_location::current());

    current_queue_id = msgget(current_key, 0666 | IPC_CREAT);
    if(current_queue_id == -1) log_sys_err_and_exit(std::experimental::source_location::current());

    if(getpid() != root_process) {
        parrent_key = ftok(parrent_ftok.first.c_str(), parrent_ftok.second);
        if(parrent_key == -1) log_sys_err_and_exit(std::experimental::source_location::current());

        parrent_queue_id = msgget(parrent_key, 0666 | IPC_CREAT);
        if(parrent_queue_id == -1) log_sys_err_and_exit(std::experimental::source_location::current());
    }

    uint32_t status = 0;
    PassBallMessage loop_msg;
    while(current_round < number_of_rounds) {
        status = msgrcv(current_queue_id, &loop_msg, sizeof(loop_msg), 4, IPC_NOWAIT);
        status = msgrcv(current_queue_id, &loop_msg, sizeof(loop_msg), 5, IPC_NOWAIT);
        if(loop_msg.pid_of_receiver == getpid())
        {    
            if(loop_msg.type == static_cast<std::underlying_type<PassBallMessageType>::type>(PassBallMessageType::forward))
            {
                if(child_data.pid == data_process_without_child.pid)
                {
                    loop_msg.type = static_cast<std::underlying_type<PassBallMessageType>::type>(PassBallMessageType::backward);
                    strcpy(loop_msg.payload, messageTypeMap.at(PassBallMessageType::backward));
                    loop_msg.pid_of_receiver = getppid();

                    if( msgsnd(parrent_queue_id, &loop_msg, sizeof(loop_msg), IPC_NOWAIT) == -1) log_sys_err_and_exit(std::experimental::source_location::current());
                    printf("im a %d no child, forward ball to parrent %d in round %d\n", getpid(), getppid(), current_round);
                    ball_send++;
                    ball_received++;
                    update_statistic(getpid());
                    current_round++;
                    continue;
                }
                loop_msg.type = static_cast<std::underlying_type<PassBallMessageType>::type>(PassBallMessageType::forward);
                strcpy(loop_msg.payload, messageTypeMap.at(PassBallMessageType::forward));
                loop_msg.pid_of_receiver = child_data.pid;

                if( msgsnd(child_queue_id, &loop_msg, sizeof(loop_msg), IPC_NOWAIT) == -1) log_sys_err_and_exit(std::experimental::source_location::current());
                printf("im a %d forward ball to %d in round %d\n", getpid(), child_data.pid, current_round);
                ball_send++;
                ball_received++;
                update_statistic(getpid());
                continue;
            }
            if(loop_msg.type == static_cast<std::underlying_type<PassBallMessageType>::type>(PassBallMessageType::backward))
            {
                if(loop_msg.pid_of_receiver == root_process)
                {
                    current_round++;
                    update_statistic(getpid());
                    if(current_round < number_of_rounds)
                    {
                        loop_msg.type = static_cast<std::underlying_type<PassBallMessageType>::type>(PassBallMessageType::forward);
                        strcpy(loop_msg.payload, messageTypeMap.at(PassBallMessageType::forward));
                        loop_msg.pid_of_receiver = child_data.pid;

                        if( msgsnd(child_queue_id, &loop_msg, sizeof(loop_msg), IPC_NOWAIT) == -1) log_sys_err_and_exit(std::experimental::source_location::current());
                        printf("im a ROOT %d forward ball to %d\n", getpid(), child_data.pid);
                        ball_send++;
                        ball_received++;
                        update_statistic(getpid());
                        continue;
                    }
                    break;
                }
                loop_msg.type = static_cast<std::underlying_type<PassBallMessageType>::type>(PassBallMessageType::backward);
                strcpy(loop_msg.payload, messageTypeMap.at(PassBallMessageType::backward));
                loop_msg.pid_of_receiver = getppid();
                current_round++;

                if( msgsnd(parrent_queue_id, &loop_msg, sizeof(loop_msg), IPC_NOWAIT) == -1) log_sys_err_and_exit(std::experimental::source_location::current());
                printf("im a %d forward ball to %d in round %d\n", getpid(), getppid(), current_round);
                ball_send++;
                ball_received++;
                update_statistic(getpid());
            }
        }
        sleep(2);
    }
    
    if(child_data.pid != data_process_without_child.pid) {
        printf("im %d and wait for child %d terminate\n", getpid(), child_data.pid);
        int child_status = -1;
        waitpid(child_data.pid, &child_status, 0);

        if(child_status == 0)
            printf("child %d terminate without error\n", child_data.pid);
        if(child_status == 1)
            printf("child %d terminate with an error\n", child_data.pid);
    }
    if(msgctl(current_queue_id, IPC_RMID, NULL) == -1) log_sys_err_and_exit(std::experimental::source_location::current());
}